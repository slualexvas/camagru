<?php

class ImagesModel
{
    const IMG_WIDTH = 320;
    const IMG_HEIGHT = 240;
    const FILE_EXTENSION = '.png';
    const FRAMES_DIR_NAME = DIR_ROOT . '/templates/img-frames';
    const MAX_FILE_SIZE = 1048576;//1024 * 1024
    const IMAGES_PER_GALLERY_PAGE = 3;

    public function getDirName($idUser)
    {
        return DIR_ROOT . '/files/' . $idUser;
    }
    public function getAvailableFrames()
    {
        $result = array();
        foreach (scandir(self::FRAMES_DIR_NAME) as $item) {
            $fileName = self::FRAMES_DIR_NAME . '/' . $item;
            if (is_file($fileName) && mb_stripos($fileName, '00.png') === false) {
                $result[] = str_replace(DIR_ROOT, '', $fileName);
            }
        }
        return $result;
    }

    public function getAlreadyCreatedImages($idUser)
    {
        $result = DbDecorator::getInstance()->fetchCol("SELECT filename FROM images WHERE id_user = {$idUser}");

        return !empty($result) ? $result : array();
    }

    public function saveImage()
    {
        $idUser = (new UsersController())->getUserIdOrThrowExceptionIfEmpty();

        $fileName = $this->getAvailableFileName($idUser);
        $path = $this->getDirName($idUser) . '/' .$fileName . self::FILE_EXTENSION;

        if (empty($_FILES['files']['name'])) {
            $image = explode('base64', $_POST['camera_shot']);
            file_put_contents($path, base64_decode($image[1]));
        } else {
            $fileNameOld = $_FILES['files']['name'];
            if (($dotIndex = mb_strrpos($fileNameOld,'.')) === false) {
                throw new ValidatorException("В имени файла {$fileNameOld} нету точки. Это ошибка!");
            }
            $extension = mb_substr($fileNameOld,$dotIndex + 1);

            $allowedExtensions = array('jpg', 'png');
            if (!in_array($extension, $allowedExtensions)) {
                throw new ValidatorException("Расширение загруженного файла '{$extension}' не входит список разрешённых расширений: ".join(', ', $allowedExtensions));
            }

            if ($_FILES['files']['size'] > self::MAX_FILE_SIZE) {
                throw new ValidatorException("Размер загруженного файла = {$_FILES['files']['size']} байт, и это больше, чем максимальный разрешённый размер ".self::MAX_FILE_SIZE);
            }

            imagepng(imagecreatefromstring(file_get_contents($_FILES['files']['tmp_name'])), $path);

            // ресайзим загруженное фото
            $imageResource = imagecreatefrompng($path);
            $imageSize = getimagesize($path);

            $resizedImageResource = imagecreatetruecolor(self::IMG_WIDTH, self::IMG_HEIGHT);
            imagecopyresampled($resizedImageResource, $imageResource, 0, 0, 0, 0, self::IMG_WIDTH, self::IMG_HEIGHT, $imageSize[0], $imageSize[1]);
            imagepng($resizedImageResource, $path);
        }

        $framePath = realpath(DIR_ROOT . '/' . $_POST['frame']);
        if (!file_exists($framePath) || mb_strpos($framePath, self::FRAMES_DIR_NAME) === false) {
            throw new ValidatorException("Файла {$_POST['frame']} не существует или он не находится в папке ".str_replace(DIR_ROOT, '', self::FRAMES_DIR_NAME));
        }

        $imageResource = imagecreatefrompng($path);
        $frameResource = imagecreatefrompng($framePath);

        $frameSize = getimagesize($framePath);
        imagecopyresampled($imageResource, $frameResource, 0, 0, 0, 0, self::IMG_WIDTH, self::IMG_HEIGHT, $frameSize[0], $frameSize[1]);
        imagepng($imageResource, $path);
        
        $db = DbDecorator::getInstance();
        $db->exec("INSERT INTO images(id_user, filename) VALUES ({$idUser}, {$fileName})");

        return $path;
    }
    public function getAvailableFileName($idUser)
    {
        $dirName = $this->getDirName($idUser);
        if (!file_exists($dirName)) {
            mkdir($dirName);
            chmod($dirName, 0777);
        }
        $n = 0;
        while (file_exists($dirName . '/' . $n . self::FILE_EXTENSION)) {
            $n++;
        }
        return $n;
    }

    public function getGalleryData($pageId)
    {
        $db = DbDecorator::getInstance();

        $limit = ($pageId - 1) * self::IMAGES_PER_GALLERY_PAGE;
        $sql = "
            SELECT i.*,
             (SELECT COUNT(*) FROM likes WHERE id_image = i.id) AS likes_count,
             (SELECT COUNT(*) FROM comments WHERE id_image = i.id) AS comments_count
            FROM images AS i
            ORDER BY i.datetime_of_creation DESC, i.id ASC
            LIMIT {$limit}, ".self::IMAGES_PER_GALLERY_PAGE;
        $result = array(
            'images' => $db->fetchAll($sql),
            'total_count' => $db->fetchOne("SELECT COUNT(*) FROM images"),
            'page_id' => $pageId,
        );
        return $result;
    }

    public function like($idUser, $imageId)
    {
        $db = DbDecorator::getInstance();

        $isLikeExists = !empty($db->exec("DELETE FROM likes WHERE id_user = {$idUser} AND id_image = {$imageId}"));
        if (!$isLikeExists) {
            $db->exec("INSERT INTO likes(id_user, id_image) VALUES({$db->quote($idUser)}, {$imageId})");
        }
    }

    public function getOneImageData($imageId)
    {
        $db = DbDecorator::getInstance();
        $sql = "
            SELECT i.*, COUNT(l.id_user) AS likes_count
            FROM images AS i
              LEFT JOIN likes AS l ON (l.id_image = i.id)
             WHERE i.id = {$imageId}";
        $imageData = $db->fetchRow($sql);

        $sql = "SELECT c.*, u.login
                FROM comments AS c
                  JOIN users AS u ON c.id_user = u.id
                WHERE c.id_image = {$imageId}
                ORDER BY id DESC";
        $commentsData = $db->fetchAll($sql);
        return array(
            'image' => $imageData,
            'comments' => $commentsData,
        );
    }

    public function validateCommentText($text)
    {
        $caption = 'Текст комментария';

        (new ValidatorCharWhiteList(ValidatorCharWhiteList::TEMPLATE_TEXT_WITHOUT_TAGS))->validate($text, $caption);
        (new ValidatorMaxLen(140))->validate($text, $caption);
        (new ValidatorMinLen(1))->validate($text, $caption);
    }

    public function addComment($userId, $imageId, $text)
    {
        $this->validateCommentText($text);

        $db = DbDecorator::getInstance();
        $db->exec("INSERT INTO comments(id_image, id_user, text) VALUES ($imageId, $userId, {$db->quote($text)})");

        $email = $db->fetchOne("SELECT u.email FROM users AS u JOIN images AS i ON i.id_user = u.id WHERE i.id = $imageId");
        myMail($email, 'Вашу фотку прокомментировали на Camagru', "id фотки = {$imageId}, текст комментария = {$text}");
    }

    public function delete($imageId, $userId)
    {
        $isOk = DbDecorator::getInstance()->exec("DELETE FROM images WHERE id = $imageId AND id_user = $userId");

        if (!$isOk) {
            throw new Exception("Либо картинки с id = {$imageId} уже нету в базе данных, либо её автор -- не вы.");
        }
    }
}