<?php
require_once __DIR__."/../const.php";
require_once DIR_ROOT."/autoload.php";
require_once DIR_ROOT."/program-files/functions.php";

function createTable($db, $tableName, $createTableQuery, $insertDataSql, &$stepIndex)
{
    $db->query("DROP TABLE IF EXISTS {$tableName}");
    $db->query($createTableQuery);
    $db->query($insertDataSql);
    
    echoStepExecuted($stepIndex, "SQL table '{$tableName}' created");
}

function echoStepExecuted(&$stepIndex, $msg)
{
    echo "Step {$stepIndex}: {$msg}\n";
    $stepIndex++;
}

$db = DbDecorator::getInstance();
$db->query("SET FOREIGN_KEY_CHECKS = 0");

$stepIndex = 1;

$usersCreateSql = "
CREATE TABLE users(
  id INT AUTO_INCREMENT,
  login VARCHAR(64) NOT NULL UNIQUE,
  password VARCHAR(64) NOT NULL,
  email VARCHAR(64) NOT NULL,
  is_confirmed INT NOT NULL DEFAULT 0,
  PRIMARY KEY(id)
)";

$usersInsertSql = "
INSERT INTO users(id, login, password, email, is_confirmed) VALUES
    (1, 'ivanov', ".$db->quote(myPasswordEncrypt('ivanovA7')).", 'angell2006@ukr.net', 1),
    (2, 'petrov', ".$db->quote(myPasswordEncrypt('petrovA7')).", 'angell2006@ukr.net', 1)";

createTable($db, 'users', $usersCreateSql, $usersInsertSql, $stepIndex);

$db->query("DROP TABLE IF EXISTS images");

$imagesCreateSql = "
CREATE TABLE images(
  id INT AUTO_INCREMENT,
  id_user INT NOT NULL,
  filename INT NOT NULL,
  datetime_of_creation DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY(id)
)";

$imagesInsertSql = "
INSERT INTO images(id, id_user, filename) VALUES
    (1, 1, 0),
    (2, 2, 0),
    (3, 2, 1),
    (4, 1, 4),
    (5, 1, 5),
    (6, 1, 6),
    (7, 1, 7),
    (8, 1, 8),
    (9, 1, 9),
    (10, 1, 10),
    (11, 1, 11),
    (12, 1, 12),
    (13, 1, 2),
    (14, 1, 3)";

createTable($db, 'images', $imagesCreateSql, $imagesInsertSql, $stepIndex);

$likesCreateSql = "
CREATE TABLE likes(
  id_image INT NOT NULL,
  id_user INT NOT NULL,
  PRIMARY KEY(id_image, id_user),
  FOREIGN KEY (id_image) REFERENCES images(id) ON DELETE CASCADE
)";

$likesInsertSql = "
INSERT INTO likes(id_image, id_user) VALUES
    (1, 1),
    (1, 2),
    (2, 2)";

createTable($db, 'likes', $likesCreateSql, $likesInsertSql, $stepIndex);

$commentsCreateSql = "
CREATE TABLE comments(
  id INT AUTO_INCREMENT,
  id_image INT NOT NULL,
  id_user INT NOT NULL,
  text VARCHAR(140),
  PRIMARY KEY(id),
  FOREIGN KEY (id_image) REFERENCES images(id) ON DELETE CASCADE
)";

$commentsInsertSql = "
INSERT INTO comments(id_image, id_user, text) VALUES
    (1, 1, 'Фсем чмоки в этом чяте!'),
    (1, 2, 'Это такое мужество -- выставить эту фотку'),
    (2, 2, 'Я тибя абажаю')";

createTable($db, 'comments', $commentsCreateSql, $commentsInsertSql, $stepIndex);

$chatMessagesCreateSql = "
CREATE TABLE chat_messages(
  id INT AUTO_INCREMENT,
  id_from INT NOT NULL,
  id_to INT NOT NULL,
  text VARCHAR(1024),
  datetime_sent DATETIME NOT NULL DEFAULT NOW(),
  datetime_read DATETIME,
  PRIMARY KEY(id)
)";

$chatMessagesInsertSql = "
INSERT INTO chat_messages(id_from, id_to, text) VALUES
    (1, 1, 'Тихо сам собою я веду беседу'),
    (1, 2, 'Петров, привет, как дела?'),
    (2, 1, 'Нормально. Иванов, а у тебя как?')";

createTable($db, 'chat_messages', $chatMessagesCreateSql, $chatMessagesInsertSql, $stepIndex);

$filesPath = DIR_ROOT . '/files';
chmod($filesPath, 0777);
echoStepExecuted($stepIndex, "chmod({$filesPath}, 0777) executed");

$db->query("SET FOREIGN_KEY_CHECKS = 1");
echo "Setup complete\n";