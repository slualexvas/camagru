<?php

class UsersModel
{
    const FIELD_MAX_LENGTH = 64;
    const FIELD_MIN_LENGTH = 3;
    const USER_LOGIN_KEY_IN_SESSION = 'user_login';
    const USER_ID_KEY_IN_SESSION = 'user_id';
    const PASSWORD_STARS = '********';

    public function login($login, $password)
    {
        $this->validate(array('login' => $login, 'password' => $password), array('login', 'password'));

        $db = DbDecorator::getInstance();
        $sql = "SELECT * FROM users WHERE login = ?";

        $stmt = $db->prepare($sql);
        $stmt->execute(array($login));
        $userData = $stmt->fetchAll();

        if (empty($userData)) {
            throw new ValidatorException((new UsersView())->getLoginFailedMessage($login, $password));
        }

        if (!password_verify($password, $userData[0]['password'])) {
            throw new ValidatorException((new UsersView())->getLoginFailedMessage($login, $password));
        }

        if (!$userData[0]['is_confirmed']) {
            throw new Exception((new UsersView())->renderTemplate("registrationFinalizeForm.php", $userData[0]));
        }

        $this->setUserLoginInSession($login);
        $this->setUserIdInSession((int)$userData[0]['id']);
    }

    public function setUserLoginInSession($login)
    {
        $_SESSION[self::USER_LOGIN_KEY_IN_SESSION] = $login;
    }

    public function getUserLoginFromSession()
    {
        return $_SESSION[self::USER_LOGIN_KEY_IN_SESSION] ?? '';
    }

    public function setUserIdInSession($id)
    {
        $_SESSION[self::USER_ID_KEY_IN_SESSION] = $id;
    }

    public function getUserIdFromSession()
    {
        return $_SESSION[self::USER_ID_KEY_IN_SESSION] ?? '';
    }

    public function logout()
    {
        unset($_SESSION[self::USER_LOGIN_KEY_IN_SESSION]);
        unset($_SESSION[self::USER_ID_KEY_IN_SESSION]);
    }

    public function validate($values, $requiredKeys)
    {
        $keys = array(
            'login' => ValidatorCharWhiteList::TEMPLATE_PARAM_NAME,
            'password' => ValidatorCharWhiteList::TEMPLATE_TEXT_WITHOUT_TAGS,
            'email' => ValidatorCharWhiteList::TEMPLATE_URL
        );

        $validatorMinLen = new ValidatorMinLen(self::FIELD_MIN_LENGTH);
        $validatorMaxLen = new ValidatorMaxLen(self::FIELD_MAX_LENGTH);

        foreach ($keys as $key => $charWhiteList) {
            if (isset($values[$key])) {
                $value = $values[$key];
            } elseif (in_array($key, $requiredKeys)) {
                throw new ValidatorException("Не заполнено обязательное поле '{$key}'");
            } else {
                continue;
            }

            if ($key != 'password') {
                $validatorMinLen->validate($value, $key);
            } else {
                $this->checkPasswordIsEnoughComplex($values['password']);
            }
            $validatorMaxLen->validate($value, $key);
            (new ValidatorCharWhiteList($charWhiteList))->validate($value, $key);
        }
    }

    public function checkPasswordIsEnoughComplex($password)
    {
        $passwordMinLen = 8;
        $msg = "Пароль должен содержать не меньше {$passwordMinLen} символов, включая хотя бы одну большую латинскую букву и хотя бы одну цифру. Ваш пароль '{$password}' ";
        if (($passwordLen = mb_strlen($password)) < $passwordMinLen) {
            throw new ValidatorException($msg."содержит {$passwordLen} символов");
        } elseif (!preg_match("/[A-Z]/u", $password)) {
            throw new ValidatorException($msg."не содержит больших латинских букв");
        } elseif (!preg_match("/\d/u", $password)) {
            throw new ValidatorException($msg."не содержит цифр");
        }
    }

    public function register()
    {
        $this->validate($_POST, array('login', 'password', 'email'));
        $db = DbDecorator::getInstance();
        
        $encryptedPassword = myPasswordEncrypt($_POST['password']);
        $sql = "INSERT INTO users(login, password, email) VALUES(
          ".$db->quote($_POST['login']).",
          ".$db->quote($encryptedPassword).",
          ".$db->quote($_POST['email'])."
        )";
        try {
            $db->exec($sql);
        } catch (DbException $e) {
            if (mb_stripos($e->getMessage(), "duplicate")) {
                throw new ValidatorException("Пользователь с логином <b>{$_POST['login']}</b> уже есть в базе данных!");
            }
        }
        
        $mailMessage = "Код для окончания регистрации: \r\n".$encryptedPassword;
        myMail($_POST['email'], "Окончание регистрации Camagru", $mailMessage);
    }

    public function inputToken()
    {
        $this->validate($_POST, array('login'));

        $db = DbDecorator::getInstance();
        $sql = "UPDATE users SET is_confirmed = 1 
                WHERE
                  login = {$db->quote($_POST['login'])}
                  AND password = {$db->quote($_POST['token'])}";
        if (!$db->exec($sql)) {//Если кол-во строк, затронутых запросом, равно нулю...
            throw new ValidatorException("Вы ввели неверный токен");
        }
    }

    public function resetPassword()
    {
        $this->validate($_POST, array('login', 'email'));

        $newPassword = $this->generateRandomPassword();
        $db = DbDecorator::getInstance();
        $sql = "UPDATE users SET password = {$db->quote(myPasswordEncrypt($newPassword))}
                WHERE login = {$db->quote($_POST['login'])}
                      AND email = {$db->quote($_POST['email'])}";
        if (!$db->exec($sql)) {//Если не нашлось ни одной записи с таким логином и имэйлом...
            throw new ValidatorException("В базе данных нету пользователя
с логином <span class='invisible-text'>{$_POST['login']}</span>
и email <span class='invisible-text'>{$_POST['email']}</span> <i>(выделите, чтобы прочесть логин и email)</i>");
        }

        myMail($_POST['email'], "Новый пароль на Camagru", $newPassword);
    }

    public function generateRandomPassword()
    {
        $result = '';

        $result .= myRandomCharFromString(ValidatorCharWhiteList::LATIN_UPPER);
        $result .= myRandomCharFromString(ValidatorCharWhiteList::DIGITS);
        for ($i = 0; $i < 6; $i++) {
            $result .= myRandomCharFromString(ValidatorCharWhiteList::DIGITS.ValidatorCharWhiteList::LATIN_LOWER.ValidatorCharWhiteList::LATIN_UPPER);
        }

        return $result;
    }

    public function getUserData($id)
    {
        $result = DbDecorator::getInstance()->fetchRow("SELECT * FROM users WHERE id = {$id}");
        if (empty($result)) {
            throw new Exception("В базе нет пользователя с id = {$id}");
        }
        return $result;
    }

    public function changeUserData($id, $values)
    {
        if ($values['password'] === self::PASSWORD_STARS) {
            unset($values['password']);
        }
        $this->validate($values, array('login', 'email'));

        $db = DbDecorator::getInstance();
        $setFields = array();
        foreach ($values as $key => $value) {
            if ($key == 'password') {
                $value = myPasswordEncrypt($value);
            }
            $setFields[] = "{$key} = {$db->quote($value)}";
        }
        try {
            $db->exec("UPDATE users SET ".join(', ', $setFields)." WHERE id = {$id}");
        } catch (DbException $e) {
            if (mb_stripos($e->getMessage(), 'duplicate') !== false) {
                throw new ValidatorException("Пользователь с логином <b>{$values['login']}</b> уже есть в базе данных!");
            } else {
                throw $e;
            }
        }
    }
}