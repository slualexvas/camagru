<div class="design-element left-col">
    <h2>Ваша вебкамера</h2>
    <div style="position: relative; overflow: hidden;
            width: <?php echo ImagesModel::IMG_WIDTH; ?>px;
            height: <?php echo ImagesModel::IMG_HEIGHT; ?>px;">
        <video autoplay id="videoCamera"
               style="background-image: url(); position: absolute;"></video>
        <img id="camera_fg"
             src="/templates/img-frames/00.png"
             style="
                     width: <?php echo ImagesModel::IMG_WIDTH; ?>px;
                     height: <?php echo ImagesModel::IMG_HEIGHT; ?>px;
                     position: absolute;"
        >
    </div>

    <canvas id="c"
            style="display:none;"
            width="<?php echo ImagesModel::IMG_WIDTH; ?>"
            height="<?php echo ImagesModel::IMG_HEIGHT; ?>"
    ></canvas>

    <form method="post"
          action="/index.php?controller=images&action=save"
          id="main_form"
          onsubmit="getFoto()"
          enctype="multipart/form-data"
    >
        <input type='hidden' name="camera_shot" id="input_camera_shot">
        <input type="submit" value="Сохранить фото с рамкой" disabled id="getFotoButton"> (щёлкните мышкой по одной из доступных рамок, чтобы эта кнопка стала активной)

        <input type='hidden' name='MAX_FILE_SIZE' value='<?php echo ImagesModel::MAX_FILE_SIZE; ?>' />
        <br>
        <input name='files' type='file'> --- Или выберите фото, которое будет обработано вместо снимка с вебкамеры
        </form>
    </form>
    <div class="design-element">
        <h2>Доступные рамки:</h2>
        <?php
        foreach ($params['frames'] as $key => $frameFileName) {
            $id = "frame{$key}Radio";
            echo "<div class='img-container'>
                    <input type='radio' name='frame' value='$frameFileName' form='main_form'  onclick='radioButtonClick(\"$frameFileName\");' id='{$id}'>
                    <label for='{$id}'><img src='$frameFileName'></label>
                  </div>";
        }
        ?>
    </div>
</div><div class="design-element right-col">
    <h2>Фото, созданные раньше:</h2>
    <?php
        $path = str_replace(DIR_ROOT, '', (new ImagesModel())->getDirName($params['id_user']));
        foreach ($params['already_created_images'] as $src) {
            echo "\n<div class='img-container'>";
            echo "\n\t<img src='{$path}/{$src}".ImagesModel::FILE_EXTENSION."'>";
            echo "\n</div>";
        }
    ?>
</div>

<script>
    var imgWidth = <?php echo ImagesModel::IMG_WIDTH; ?>;
    var imgHeight = <?php echo ImagesModel::IMG_HEIGHT; ?>;
    
    function onError(err) {
        alert(err.message);
    }
    
    function onGetUserMedia(stream) {
        var video = document.querySelector('#videoCamera');
        // Older browsers may not have srcObject
        if ("srcObject" in video) {
            video.srcObject = stream;
        } else {
            // Avoid using this in new browsers, as it is going away.
            video.src = window.URL.createObjectURL(stream);
        }
    }

    function getFoto() {
        var video = document.querySelector('#videoCamera');
        var canvas = document.querySelector('canvas');
        canvas.getContext("2d").drawImage(video, 0, 0, <?php echo ImagesModel::IMG_WIDTH; ?>, <?php echo ImagesModel::IMG_HEIGHT; ?>);

        var imageDataURL = canvas.toDataURL('image/png');
        document.getElementById('input_camera_shot').setAttribute('value', imageDataURL);
    }

    function radioButtonClick(src) {
        document.getElementById("camera_fg").setAttribute('src', src);
        document.getElementById("getFotoButton").removeAttribute('disabled');
    }
    
    var userMediaOptions = {video: {width: imgWidth, height: imgHeight}};

    //Это отработает в Firefox
    var userMediaObject = navigator.mediaDevices;
    if (userMediaObject.getUserMedia !== undefined) {
        userMediaObject.getUserMedia(userMediaOptions)
            .then(onGetUserMedia)
            .catch(onError);
    } else {
        //Это отработает в Хроме
        userMediaObject = navigator;
        if (userMediaObject.getUserMedia !== undefined) {
            userMediaObject.getUserMedia(userMediaOptions, onGetUserMedia, onError);
        }
    }
    
</script>