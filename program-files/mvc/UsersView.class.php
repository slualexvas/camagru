<?php
class UsersView extends BaseView
{
    public function strLoginFormIfNotAuthorized()
    {
        return $this->renderTemplate('loginFormIfNotAuthorized.php');
    }

    public function strLoginFormIfAuthorized($login)
    {
        $params = array('login' => $login);
        return $this->renderTemplate('loginFormIfAuthorized.php', $params);
    }
    
    public function getLoginFailedMessage($login, $password)
    {
        return "В базе данных нету пользователя
с логином <span class='invisible-text'>{$login}</span>
и паролем <span class='invisible-text'>{$password}</span> <i>(выделите, чтобы прочесть логин и пароль)</i>";
    }
}