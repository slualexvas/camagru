<?php
$dirName = str_replace(DIR_ROOT, '', (new ImagesModel())->getDirName($params['image']['id_user']));
$path = $dirName.'/'.$params['image']['filename'].ImagesModel::FILE_EXTENSION;
echo "<img src='$path'>";

$userId = (new UsersModel())->getUserIdFromSession();
$likeButton = empty($userId) ? '' : "
        <form method='post' 
              action='/index.php?controller=images&action=like&image_id={$params['image']['id']}'
              style='display: inline-block'
        >
            <input type='submit' value='Лайкнуть'>
        </form>
    ";

echo "\n\t<br>Лайков: {$params['image']['likes_count']} {$likeButton}";

if (!empty($params['comments'])) {
    echo "\n<h2>Комментарии (свежие сверху):</h2>";
    foreach ($params['comments'] AS $comment) {
        echo "<p><b>{$comment['login']}</b>: {$comment['text']}</p>";
    }
}

if (!empty($userId)) {
    echo "
        <form action='/index.php?controller=images&action=comment&image_id={$params['image']['id']}' method='post'>
            <textarea name='text' placeholder='Всех очень интересует ваше мнение. Напишите же его, но не превышайте 140 символов' cols='40' rows='4'></textarea>
            <br><input type='submit' value='Высказать мнение'>
        </form>";
}

?>


