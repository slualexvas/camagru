<?php
class UsersController extends BaseController
{
    public function strLoginForm()
    {
        $authorizedUserLogin = (new UsersModel())->getUserLoginFromSession();

        $view = new UsersView();
        return empty($authorizedUserLogin) ? $view->strLoginFormIfNotAuthorized() : $view->strLoginFormIfAuthorized($authorizedUserLogin);
    }

    public function actionLogin()
    {
        $login = $_POST['login'] ?? '';
        $password = $_POST['password'] ?? '';

        (new UsersModel())->login($login, $password);
        return "Вы успешно залогинились!";
    }

    public function actionLogout()
    {
        (new UsersModel())->logout();
        return "Вы успешно разлогинились!";
    }

    public function actionRegisterForm()
    {
        return (new UsersView())->renderTemplate("registerForm.php");
    }

    public function actionRegister()
    {
        (new UsersModel())->register();
        return (new UsersView())->renderTemplate("registrationFinalizeForm.php", $_POST);
    }

    public function actionInputToken()
    {
        (new UsersModel())->inputToken();
        return "Ваш аккаунт подтверждён. Можете логиниться и работать.";
    }

    public function actionResetPasswordForm()
    {
        return (new UsersView())->renderTemplate("resetPasswordForm.php");
    }

    public function actionResetPassword()
    {
        (new UsersModel())->resetPassword();
        return "Пароль успешно изменён! Вам на почту отправлено письмо с новым паролем.";
    }

    public function getUserIdOrThrowExceptionIfEmpty()
    {
        $id = (new UsersModel())->getUserIdFromSession();
        if (empty($id)) {
            throw new EmptyUserIdException();
        }
        return $id;
    }

    public function actionChangeUserDataForm()
    {
        $userId = $this->getUserIdOrThrowExceptionIfEmpty();
        $params = (new UsersModel())->getUserData($userId);
        $params['password'] = UsersModel::PASSWORD_STARS;
        return ((new BaseView())->renderTemplate('changeUserDataForm.php', $params));
    }

    public function actionChangeUserData()
    {
        $id = $this->getUserIdOrThrowExceptionIfEmpty();
        (new UsersModel())->changeUserData($id, $_POST);
        return "Ваши данные успешно изменены";
    }
}

class EmptyUserIdException extends Exception
{
    public function __construct(string $message = "Это действие доступно только авторизованным пользователям. Пожалуйста, залогиньтесь.", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}