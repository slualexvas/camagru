<?php

class ChatController extends BaseController
{
    public function actionUserList(){
        $params = ['users' => DbDecorator::getInstance()->fetchAll("SELECT * FROM users")];
        return (new BaseView())->renderTemplate("userList.php", $params);
    }
    
    public function actionChatWith(){
        $params = (new ChatModel())->getDataForChat(
            (new UsersController())->getUserIdOrThrowExceptionIfEmpty(),
            (int)($_GET['id'] ?? 0)
        );
        
        return (new BaseView())->renderTemplate("chatWith.php", $params);
    }

    public function actionSendMessage()
    {
        (new ChatModel())->sendMessage($_POST['message'] ?? '');
        myPhpRedirect($_SERVER['HTTP_REFERER']);
    }

    public function actionAjaxGetMessages()
    {
        $data = (new ChatModel())->getDataForChat(
            (new UsersController())->getUserIdOrThrowExceptionIfEmpty(),
            (int)($_GET['id'] ?? 0)
        );
        echo json_encode($data);die;
    }
}