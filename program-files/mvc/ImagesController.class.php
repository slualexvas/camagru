<?php
class ImagesController extends BaseController
{
    public function actionCreate()
    {
        $idUser = (new UsersController())->getUserIdOrThrowExceptionIfEmpty();

        $imagesModel = new ImagesModel();
        $params = array(
            'frames' => $imagesModel->getAvailableFrames(),
            'already_created_images' => $imagesModel->getAlreadyCreatedImages($idUser),
            'id_user' => $idUser,
        );
        return (new BaseView())->renderTemplate("createImageForm.php", $params);
    }

    public function actionSave()
    {
        $imgPath = (new ImagesModel())->saveImage();
        return "<img src='".str_replace(DIR_ROOT, '', $imgPath)."'>
                <p>Картинка успешно создана! Предлагаем посетить
                    <a href='/index.php?controller=images&action=gallery'>галерею</a>.</p>";
    }

    public function actionGallery()
    {
        if (isset($_GET['page'])) {
            $pageId = (int)$_GET['page'];
            if ($pageId <= 0) {
                throw new Exception("Некорректный номер страницы: ".htmlspecialchars($_GET['page']));
            }
        } else {
            $pageId = 1;
        }

        $galleryData = (new ImagesModel())->getGalleryData($pageId);
        if (empty($galleryData['images'])) {
            throw new Exception("Либо галерея пуста, либо указан некорректный номер страницы");
        }
        return (new BaseView())->renderTemplate('gallery.php', $galleryData);
    }

    public function actionLike()
    {
        $imageId = $_GET['image_id'] ?? '';
        (new ValidatorInt())->validate($imageId, 'image_id');
        $idUser = (new UsersController())->getUserIdOrThrowExceptionIfEmpty();

        (new ImagesModel())->like($idUser, $imageId);
        myPhpRedirect($_SERVER['HTTP_REFERER']);
    }

    public function actionShowOne()
    {
        $imageId = $_GET['id'] ?? '';
        (new ValidatorInt())->validate($imageId, 'image_id');

        $imageData = (new ImagesModel())->getOneImageData($imageId);
        if (empty($imageData)) {
            throw new Exception("Фотографии с id = {$imageId} не найдено в базе данных");
        }
        return (new BaseView())->renderTemplate("oneImage.php", $imageData);
    }

    public function actionComment()
    {
        $userId = (new UsersController())->getUserIdOrThrowExceptionIfEmpty();
        $imageId = $_GET['image_id'] ?? '';
        (new ValidatorInt())->validate($imageId, 'image_id');
        $text = $_POST['text'] ?? '';

        (new ImagesModel())->addComment($userId, $imageId, $text);
        myPhpRedirect($_SERVER['HTTP_REFERER']);
    }

    public function actionDelete()
    {
        $userId = (new UsersController())->getUserIdOrThrowExceptionIfEmpty();
        $imageId = $_GET['image_id'] ?? '';
        (new ValidatorInt())->validate($imageId, 'image_id');

        (new ImagesModel())->delete($imageId, $userId);
        myPhpRedirect($_SERVER['HTTP_REFERER']);
    }
}