<?php
require_once __DIR__."/../const.php";
include_once DIR_ROOT."/config/database.php";

class DbDecorator
{
    private $dbLink = null;
    private static $instance = null;//Реализация паттерна Singletone

    private function __clone(){}//Реализация паттерна Singletone
    private function __wakeup(){}//Реализация паттерна Singletone

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $pdoOptions = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        $this->dbLink = new PDO(DB_DSN, DB_USER, DB_PASSWORD, $pdoOptions);
        try {
            $this->exec("USE ".DB_NAME);
        } catch (DbException $e) {//если база данных не существует
            $this->exec("CREATE DATABASE ".DB_NAME);
            $this->exec("USE ".DB_NAME);
        }
    }

    public function quote($value, $parameter_type = PDO::PARAM_STR)
    {
        return $this->dbLink->quote($value, $parameter_type);
    }

    public function exec($strQuery)
    {
        try {
            return $this->dbLink->exec($strQuery);
        } catch (Exception $e) {
            throw new DbException($e->getMessage(), $e->getCode(), $e, $strQuery);
        }
    }

    public function query($strQuery)
    {
        try {
            return $this->dbLink->query($strQuery);
        } catch (Exception $e) {
            throw new DbException($e->getMessage(), $e->getCode(), $e, $strQuery);
        }
    }

    public function fetchAll($strQuery)
    {
        $queryResult = $this->query($strQuery);
        return $queryResult->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchRow($strQuery)
    {
        $queryResult = $this->query($strQuery);
        return $queryResult->fetch(PDO::FETCH_ASSOC);
    }

    public function fetchCol($strQuery, $colIndex = 0)
    {
        $rawResult = $this->fetchAll($strQuery);
        if (empty($rawResult)) {
            return false;
        }

        $colAssocIndex = array_keys($rawResult[0])[$colIndex] ?? false;
        if ($colAssocIndex === false) {
            return false;
        }

        $col = array();
        foreach ($rawResult as $row) {
            $col[] = $row[$colAssocIndex];
        }
        return $col;
    }

    public function fetchOne($strQuery, $colIndex = 0)
    {
        $rawResult = $this->fetchRow($strQuery);
        if (empty($rawResult)) {
            return false;
        }

        $colAssocIndex = array_keys($rawResult)[$colIndex] ?? false;
        if ($colAssocIndex === false) {
            return false;
        }

        return $rawResult[$colAssocIndex];
    }

    public function prepare($strQuery)
    {
        return $this->dbLink->prepare($strQuery);
    }
}

class DbException extends Exception
{
    protected $strQuery;

    public function __construct($message = "", $code = 0, Throwable $previous = null, $strQuery)
    {
        parent::__construct($message, (int)$code, $previous);
        $this->strQuery = $strQuery;
    }

    public function getQuery()
    {
        return $this->strQuery;
    }
}
