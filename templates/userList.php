<h1>Список пользователей</h1>

<table border="1">
    <thead>
        <th>Имя пользователя</th>
        <th>Ссылка на чат</th>
    </thead>
<?php
    foreach ($params['users'] as $user) {
        echo "<tr>";
        echo "<td>{$user['login']}</td>";
        echo "<td><a href='/index.php?controller=chat&action=chatWith&id={$user['id']}'>Перейти к чату</a></td>";
        echo "</tr>";
    }
?>
</table>
