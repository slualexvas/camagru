<h1>Отдельное фото</h1>
<?php
$extension = ImagesModel::FILE_EXTENSION;

$disabledButtonTitle = (empty((new UsersModel())->getUserLoginFromSession())) ? 'Нужно залогиниться, чтобы ставить лайки' : 'Вы уже ставили лайк этой фотке';

$imgData = $params[0];

$disabled = $imgData['can_you_like'] === '1' ? '' : "disabled title='{$disabledButtonTitle}'";
$likeButton = "
        <form method='post' 
              action='/index.php?controller=images&action=like&image_id={$imgData['id']}'
              style='display: inline-block'
        >
            <input type='submit' value='Лайкнуть' $disabled>
        </form>
    ";

echo "\n<div class='gallery-item design-element'>";
echo "\n\t<img src='/files/{$imgData['login']}/{$imgData['filename']}{$extension}'>";
echo "\n\tЛайков: {$imgData['likes_count']} {$likeButton}";
echo "\n</div>";


?>
