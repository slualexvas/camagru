<?php
class BaseController {
    public static function factory()
    {
        $controllerGetParam = $_GET['controller'] ?? 'images';
        $className = ucfirst($controllerGetParam) . 'Controller';
        if (class_exists($className)) {
            return new $className();
        } else {
            throw new Exception("Class '{$className}' not found");
        }
    }

    public function getDefaultAction()
    {
        return "gallery";
    }

    public function doAction()
    {
        $action = $_GET['action'] ?? $this->getDefaultAction();
        $methodName = 'action' . ucfirst($action);
        $fullMethodName = get_class($this).'::'.$methodName;

        if (method_exists($this, $methodName)) {
            $page = $this->$methodName();
            if (is_string($page)) {
                $page = new Page($page);
            }
            if (!is_a($page, 'Page')) {
                throw new Exception("Method {$fullMethodName} returned not page. Returned value: <pre>".var_export($page, true)."</pre>");
            }
            return $page;
        } else {
            throw new Exception("Method ".get_class($this)."::{$methodName} not found");
        }
    }

    public function actionIndex()
    {
        return __METHOD__;
    }
}