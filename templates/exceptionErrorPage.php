<h1><?php echo is_a($params, "ValidatorException") ? "Вы ввели неверные данные" : "Ошибка"; ?></h1>
<p>Сообщение об ошибке:</p>
<div class="design-element" style="background-color: rgba(255,0,0,0.1)">
<?php echo $params->getMessage(); ?>
</div>
<?php if(is_a($params, "ValidatorException")) { ?>
<p><b>Camagru</b> предлагает вам нажать ссылку <a href="<?php echo myJavascriptBackLinkHref();?>">Назад</a> и исправить данные.</p>
<?php } ?>
