<h1>Сброс пароля</h1>
<form method='post' action='/index.php?controller=users&action=resetPassword'>
    <input name='login' size=32 placeholder='Логин' maxlength="<?php echo UsersModel::FIELD_MAX_LENGTH; ?>"> &mdash; Введите логин, для которого нужно сбросить пароль<br>
    <input name='email' size=32  type='email' placeholder='email' maxlength="<?php echo UsersModel::FIELD_MAX_LENGTH; ?>"> &mdash; Введите email, на который нужно отправить новый пароль<br>
    <input type='submit' value='Зарегистрироваться'>
    <p><b>Внимание!</b> Пара "логин-email" должна быть именно такой же, которая указана при регистрации. Если эта пара не такая же, то пароль сброшен не будет!</p>
</form>