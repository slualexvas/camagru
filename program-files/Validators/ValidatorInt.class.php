<?php

class ValidatorInt extends Validator
{
    public function validate($value, $caption)
    {
        parent::validate($value, $caption);
        if (!myIsInt($value)) {
            throw new ValidatorException("Value '$caption' = '".htmlspecialchars($value)."' is not integer");
        }
    }
}