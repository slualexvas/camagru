<?php

class ValidatorLen extends Validator
{
    public $len = 0;
    protected $errLenCompareCharacter = '';

    public function __construct($len)
    {
        parent::__construct();

        if (!myIsInt($len) || $len <= 0) {
            throw new Exception("len = '$len' but must be a positive integer");
        }
        $this->len = $len;
    }

    protected function testLen($valueLen) {
        return true;
    }

    public function validate($value, $caption)
    {
        parent::validate($value, $caption);
        $valueLen = mb_strlen($value);
        if (!$this->testLen($valueLen)) {
            throw new ValidatorException("Value '$caption' = '$value' has len = $valueLen (must be {$this->errLenCompareCharacter} {$this->len})");
        }
    }
}