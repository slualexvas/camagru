<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once "const.php";
require_once "autoload.php";
require_once DIR_ROOT."/program-files/functions.php";

session_start();
try {
    $controllerObj = BaseController::factory();
    $page = $controllerObj->doAction();
} catch (Exception $e) {
    $content = (new BaseView())->renderTemplate("exceptionErrorPage.php", $e);
    $page = new Page($content, "Error");
}
include "templates/main-template.php";