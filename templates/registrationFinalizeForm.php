Просим вас сходить на свой email
    <b><?php echo $params['email']; ?></b>,
найти письмо от <b>Camagru</b> с токеном для окончания регистрации, ввести токен в текстовое поле ниже, и нажать кнопку <b>Окончить регистрацию</b>:

<form action="/index.php?controller=users&action=inputToken" method="post">
    <input type="text" name="token" placeholder="Введите токен для окончания регистрации здесь" maxlength="64" size="64">
    <input type="hidden" name="login" value="<?php echo $params['login']; ?>">
    <br>
    <input type="submit" value="Окончить регистрацию">
</form>
