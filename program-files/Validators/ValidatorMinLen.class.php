<?php

class ValidatorMinLen extends ValidatorLen
{
    protected $errLenCompareCharacter = '>=';
    protected function testLen($valueLen)
    {
        return $valueLen >= $this->len;
    }
}