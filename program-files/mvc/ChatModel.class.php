<?php

class ChatModel
{
    private function getMessages($idFrom, $idTo)
    {
        $sqlWhereOr = [
            "id_from = {$idFrom} AND id_to = {$idTo}",
            "id_from = {$idTo} AND id_to = {$idFrom}",
        ];
        
        $sqlWhere = [
            '(' . join(' OR ', $sqlWhereOr) . ')',
        ];
        
        $messages = DbDecorator::getInstance()->fetchAll(
            "SELECT * FROM chat_messages
                        WHERE " . join(' AND ', $sqlWhere) . "
                        ORDER BY datetime_sent"
        );
        $this->setDatetimeReadInMessages($idFrom, $messages);
        return $messages;
    }
    
    public function getDataForChat($idFrom, $idTo)
    {
        $db = DbDecorator::getInstance();
        
        $params = [
            'id_from'    => $idFrom,
            'id_to'      => $idTo,
            
            'login_from' => (new UsersModel())->getUserLoginFromSession(),
            'login_to'   => $db->fetchOne("SELECT login FROM users WHERE id = {$idTo}"),
        ];
        
        if (empty($params['login_to'])) {
            throw new Exception("Вы пытаетесь чатиться с несуществующим пользователем");
        }
        
        $params['messages'] = $this->getMessages($idFrom, $idTo);
        
        return $params;
    }
    
    private function setDatetimeReadInMessages($idFrom, &$messages) {
        $idsToUpdate = [];
        $curDate = date('Y-m-d H:i:s');
        foreach ($messages as $key => $message) {
            if (empty($message['datetime_read']) && $message['id_to'] == $idFrom) {
                $idsToUpdate[] = $message['id'];
                $messages[$key]['datetime_read'] = $curDate;
            }
        }

        if (!empty($idsToUpdate)) {
            DbDecorator::getInstance()->query("UPDATE chat_messages SET datetime_read = '{$curDate}' WHERE id IN (".join(', ', $idsToUpdate).")");
        }
    }

    public function sendMessage(string $message)
    {
        if (mb_strlen($message) == 0) {
            throw new Exception("Нельзя отправлять пустые сообщения");
        }

        $db = DbDecorator::getInstance();

        $sql = "INSERT INTO chat_messages(id_from, id_to, text) VALUES(?, ?, ?)";
        $stmt = $db->prepare($sql);

        $idFrom = (new UsersController())->getUserIdOrThrowExceptionIfEmpty();
        $idTo = (int)($_POST['id_to'] ?? 0);
        $stmt->execute([$idFrom, $idTo, $message]);
    }
}