<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?php echo $page->getTitle(); ?></title>
    <link rel="stylesheet" href="/templates/main-template.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="site-header design-element">Camagru</div>
<div class="menu design-element">
    <a href="/">Галерея</a>
    <a href="/index.php?controller=chat&action=userList">Чат</a>
</div>
<div class="content design-element">
    <?php echo $page->content; ?>
</div>
<div class="users-data design-element">
    <?php echo (new UsersController())->strLoginForm(); ?>
</div>
</body>
</html>