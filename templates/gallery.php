<h1>Галерея</h1>
<?php
$extension = ImagesModel::FILE_EXTENSION;

$userId = (new UsersModel())->getUserIdFromSession();
foreach ($params['images'] as $imgData) {
    $likeButton = empty($userId) ? '' : "
        <form method='post' 
              action='/index.php?controller=images&action=like&image_id={$imgData['id']}'
              style='display: inline-block'
        >
            <input type='submit' value='Лайкнуть'>
        </form>
    ";

    $deleteButton = ($userId != $imgData['id_user']) ? '' : "
        <form method='post' 
              action='/index.php?controller=images&action=delete&image_id={$imgData['id']}'
        >
            <input type='submit' value='Удалить фотку'>
        </form>
    ";

    echo "\n<div class='gallery-item design-element'>";
    echo "\n\t<img src='/files/{$imgData['id_user']}/{$imgData['filename']}{$extension}'>";
    echo "\n\tЛайков: {$imgData['likes_count']} {$likeButton}";
    echo "\n\t<a href='/index.php?controller=images&action=showOne&id={$imgData['id']}'>Комментарии ({$imgData['comments_count']})</a>";
    echo "\n\t{$deleteButton}";
    echo "\n</div>";
}

echo "\n<div class='design-element'>";

$galleryPageAddr = "/index.php?controller=images&action=gallery";
$pageCount = ceil($params['total_count'] / ImagesModel::IMAGES_PER_GALLERY_PAGE);
echo "\nСтраницы: ";
for ($i = 1; $i <= $pageCount; $i++) {
    echo $i == $params['page_id'] ? " {$i} " : "<a href='{$galleryPageAddr}&page={$i}'>{$i}</a> ";
}
echo "\n</div >";

?>
