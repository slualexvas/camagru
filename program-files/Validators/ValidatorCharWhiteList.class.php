<?php

class ValidatorCharWhiteList extends Validator
{
    protected $charWhiteList = '';

    const TEMPLATE_PARAM_NAME = self::DIGITS
                                .self::LATIN_LOWER
                                .self::ADDITIONAL_PARAM_NAME_CHARS;
    const TEMPLATE_URL = self::DIGITS
                        .self::LATIN_LOWER
                        .self::LATIN_UPPER
                        .self::ADDITIONAL_PARAM_NAME_CHARS
                        .self::ADDITIONAL_EMAIL_CHARS
                        .self::ADDITIONAL_URL_CHARS
                        .self::CYRILLIC_UPPER
                        .self::CYRILLIC_LOWER
                        .'()!';
    const TEMPLATE_EMAIL = self::DIGITS
                            .self::LATIN_LOWER
                            .self::LATIN_UPPER
                            .self::ADDITIONAL_PARAM_NAME_CHARS
                            .self::ADDITIONAL_EMAIL_CHARS;
    const TEMPLATE_TEXT_WITHOUT_TAGS = self::DIGITS
                                        .self::LATIN_LOWER
                                        .self::LATIN_UPPER
                                        .self::CYRILLIC_LOWER
                                        .self::CYRILLIC_UPPER
                                        .self::ADDITIONAL_PARAM_NAME_CHARS
                                        .self::ADDITIONAL_EMAIL_CHARS
                                        .self::ADDITIONAL_PUNCTUATION_CHARS
                                        .self::ADDITIONAL_URL_CHARS
                                        .self::ADDITIONAL_TEXT_CHARS;

    const DIGITS = '0123456789';
    const LATIN_UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const LATIN_LOWER = 'abcdefghijklmnopqrstuvwxyz';
    const CYRILLIC_UPPER = 'АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЮЯЁЪЇЄЭЫІ';
    const CYRILLIC_LOWER = 'абвгдежзийклмнопрстуфхцчшщьюяёъїєэыі';

    const ADDITIONAL_PARAM_NAME_CHARS = '-_';
    const ADDITIONAL_EMAIL_CHARS = '@.';
    const ADDITIONAL_URL_CHARS = ':/?&=%+,#';
    const ADDITIONAL_PUNCTUATION_CHARS = ',!?()«»;\"\' –+—';
    const ADDITIONAL_TEXT_CHARS = '`@|?!%\n\r';

    public function __construct($charWhiteList)
    {
        parent::__construct();
        $this->charWhiteList = $charWhiteList;
    }

    public function validate($value, $caption)
    {
        parent::validate($value, $caption);

        $valueLen = mb_strlen($value);
        for ($i = 0; $i < $valueLen; $i++) {
            $valueChar = mb_substr($value, $i, 1);
            if (mb_strpos($this->charWhiteList, $valueChar) === false) {
                $dataBeforeRed = mb_substr($value, 0, $i);
                $dataAfterRed = mb_substr($value, $i + 1);
                $dataWithRed = $dataBeforeRed."<span style='background-color: red'>".$valueChar."</span>".$dataAfterRed;
                throw new ValidatorException("В атрибуте '$caption' найден запрещённый символ. Он выделен красным цветом в следующем тексте: ".$dataWithRed.". Список разрешенных символов выделен зелёным цветом: <span style='color: green;word-wrap: break-word'>".$this->charWhiteList."</span>");
            }
        }
    }
}