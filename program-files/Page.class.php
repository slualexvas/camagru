<?php
class Page {
    public $content;
    public $title;

    public function __construct($content = '', $title = '')
    {
        $this->content = $content;
        $this->title = $title;
    }

    public function getTitle()
    {
        if (!empty($this->title)) {
            return $this->title;
        } else {
            $controller = $_GET['controller'] ?? '';
            $action = $_GET['action'] ?? '';
            return $controller.' '.$action;
        }
    }
}