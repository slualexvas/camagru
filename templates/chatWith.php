<?php
    echo "<h1>Чат: вы ({$params['login_from']}) общаетесь с пользователем {$params['login_to']}</h1>";
    
    echo "<div class='messages_container'>";
    
    echo "</div> <!-- End of messages_container -->";
?>

<form id='myform' method="post" action="/index.php?controller=chat&action=sendMessage" style="background-color: rgba(0,0,0,0.1); padding: 0.5em; margin: 0.5em;" onsubmit="formSubmit();return false;">
    <h3>Отправить сообщение</h3>
    <input id="input_message" type="text" name="message" placeholder="Введите сообщение и нажмите Enter" style="width: 100%; box-sizing: border-box;" />
    <input type="hidden" name="id_to" value="<?php echo $params['id_to']; ?>" />
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script>
    function printComments(params) {
        var messagesContainer = $('.messages_container');

        // 1) Удалить весь HTML из container;
        messagesContainer.empty();
        // 2) В цикле по сообщениям подобавлять HTML.

        var html = "";
        params.messages.forEach(function (message) {
            var isMessageFromMe = (message.id_from == params.id_from);
            var messageClass = isMessageFromMe ? 'from_me' : 'to_me';
            var messageLogin = isMessageFromMe ? 'Я' : params.login_to;

            if (message.datetime_read == null) {
                message.datetime_read = '(Ещё не прочитан)';
            }

            html += "" +
                "<div class='message " + messageClass + "'>" +
                "<div class='msg_login'>" + messageLogin + ":</div> " + message.text + " <br>" +
                "<div class='datetime'>(<b>Отправлен:</b> " + message.datetime_sent + ")," +
                "(<b>Прочитан:</b> " + message.datetime_read + "),</div>" +
                "</div> <!-- end of message -->";
        });

        messagesContainer.append(html);
    }

    function loadMessages() {
        idTo = <?php echo $params['id_to']; ?>;
        $.get('/index.php?controller=chat&action=ajaxGetMessages&id=' + idTo, printComments, 'json');
    }

    loadMessages();
    setInterval(loadMessages, 1000);

    function formSubmit() {
        $.post('/index.php?controller=chat&action=sendMessage&id=' + idTo, $("#myform").serialize());
        loadMessages();
        $("#input_message").val('');
    }
</script>