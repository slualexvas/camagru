<?php
function myIsInt($value) {
    return is_numeric($value) && floatval($value) == intval($value);
}

function myCheckDatetimeByFormat($value, $format) {
    $date = DateTime::createFromFormat($format, $value);
    return !empty($date) && ($value == ($date->format($format)));
}

function myIsDate($value) {
    return myCheckDatetimeByFormat($value, DATE_FORMAT) || $value == '0000-00-00';
}

function myIsDatetime($value) {
    return myCheckDatetimeByFormat($value, DATETIME_FORMAT) || $value == '0000-00-00 00:00:00';
}

function myPasswordEncrypt($value) {
    return password_hash($value, PASSWORD_DEFAULT);
}

function myJavascriptBackLinkHref() {
    return 'javascript:history.go(-1);';
}

function myMail($to,$subject,$message,$from='') {
    $message="<html><body>".$message."</body></html>";
    if (empty($from)) {
        $from = ADMIN_EMAIL;
    }
    
    $headers="Content-Type: text/html; charset=utf-8";
    $headers.="\r\n"."From: $from
		\r\n"."Reply-To: $from";
    if (!mail($to, $subject, $message, $headers))
    {
        $params = array(
            'to' => $to,
            'subject' => $subject,
            'message' => $message,
            'headers' => $headers
        );
        echo "Функция php mail(to, subject, message, headers) вернула false! Параметры: <pre>".var_export($params, true)."</pre>";
    }
}

function myRandomCharFromString($s) {
    return mb_substr($s, rand(0, mb_strlen($s) - 1), 1);
}

function myPhpRedirect($href) {
    header("Location: {$href}",TRUE);
}